# Copyright 2018 Nick Howell
# SPDX-License-Identifier: AGPL-3.0-or-later

ns=$(shell seq 1 7)
targets = $(foreach n,$(ns),crh_CYRL.ngrams.$(n).tsv) crh_CYRL2LAT.tsv crh_CYRL.txt wikipedia.words ibt-bible-crh.words
sources = ibt-sword-crh.zip medeniye-ru-crh-cyr.zip medeniye-ru-crh-lat.zip crhwiki-pages-articles.xml.bz2
zip_sources = $(filter %.zip,$(sources))
dictfiles = medeniye-ru-crh-cyr/ru_crh_cyr/ru_crh_cyr.dsl medeniye-ru-crh-lat/ru_crh_lat/ru_crh_lat.dsl

all: $(targets)

# include dependency files for each source zip
$(foreach source,$(zip_sources),$(eval include $(source).make))

$(sources): %:
	git annex get $@

# generate an extract rule for each file in the zip
$(subst .zip,.zip.make,$(zip_sources)): %.zip.make: %.zip
	zipinfo -1 $< | while read l; do \
		echo -e "$*/$$l: $<\n\tunzip -d $* -o -DD $< $$l"; \
	done > $@

# unpack entire %.zip to %/
$(subst .zip,,$(zip_sources)): %: %.zip %.zip.make
	mkdir -p $@
	touch $@
	unzip -d $@ -o -DD $<

$(subst .dsl,.dsl.utf8,$(dictfiles)): %.utf8: %
	uconv -f utf16le -t utf8 $< | sed 's/\r//g' > $@
# merge lines starting with whitespace to the line above,
# separate with \t
#
# gives dictionary format of <word>\t<def1>\t<def2>...
# necessary so we can correct sort the dictionary
# (since lat and cyrl are not in the same order)
$(subst .dsl,.dsl.utf8.lines,$(dictfiles)): %.utf8.lines: %.utf8
	<$< sed -n -e '/^\S/ { x; s/\r\?\n\s*/\t/g; p; }; /^\s/H; $$ { H; x; s/\r\?\n\s*/\t/g; p; }' \
		| sort -k1 -t'	' >$@
# clean (most) annotations which are not consistent between
# the two dictionaries; result is 22/48500 annotations
# may exist other errors
$(subst .dsl,.dsl.utf8.clean,$(dictfiles)): %.utf8.clean: %.utf8.lines
	<$< sed -e 's/\r//g' \
		-e 's@\s*\[p\].*\[/p\]\s*@@g' \
		-e 's@\[i\]\(.*\)\[/i\]@\1@g' \
		-e 's@\s*\[ref\].*\[/ref\]\s*@@g' \
		-e 's@\s*\[c[ a-z]*\]\(.*\)\[/c\]\s*@@g' >$@

# generate transliteration tsv from word diff between
# latin and cyrillic definitions
medeniye-crh.tsv: $(subst .dsl,.dsl.utf8.clean,$(dictfiles))
	dwdiff -3 -P -d ' ' $^ \
		| grep -v '^=*$$' \
		| sed -e '1,$$N' -e 's/\s*\[-//g' -e 's/+}\s*/\n/g' -e 's/-\]\s*{+/\t/g' \
		| grep . | grep -v '[[:punct:]]' | sed 1d >$@

crh_CYRL2LAT.tsv: medeniye-crh.tsv transpose
	./transpose $< > $@

ibt-bible-crh.txt: ibt-sword-crh
	( cd $<; (echo -n "Genesis 1:1 ; Genesis 1:2 ; "; diatheke -b CRT -s regex -k .) \
		| parallel -k -d';' -n 100 'diatheke -b CRT -o h -f plain -k {}' ) \
		| sed "s/^[a-zA-Z ]* [0-9]\+:[0-9]\+: //" \
		| grep -v '^(CRT)$$' > $@
ibt-bible-crh.words: ibt-bible-crh.txt segment
	./segment $< > $@
crh_CYRL.txt: ibt-bible-crh.words
	cp $< $@
ibt-bible-crh.%grams: ibt-bible-crh.words n-grams
	./n-grams $* < $< > $@
ibt-bible-crh.%grams.counts: ibt-bible-crh.%grams n-gram-counts
	./n-gram-counts $< > $@
ibt-bible-crh.%weight: ibt-bible-crh.%grams ibt-bible-crh.%grams.counts counts weights
	./counts $< | ./weights $<.counts > $@
crh_CYRL.ngrams.%.tsv: ibt-bible-crh.%weight transpose
	./transpose $< > $@
WikiExtractor.py:
	wget https://svn.code.sf.net/p/apertium/svn/trunk/apertium-tools/$@
	chmod +x $@
wikipedia.txt: crhwiki-pages-articles.xml.bz2 WikiExtractor.py
	./WikiExtractor.py --infn $< >/dev/null
	mv wiki.txt $@
wikipedia.words: wikipedia.txt
	sed 's/[[:space:]]\+/\n/g' <$< | grep '[a-zA-Z]' | tr -cs '[:alpha:]-' '[\n*]' >$@
	#| tr -d '[[:punct:]]' | tr -d '[“”]' >$@
